<?php

namespace App\Http\Controllers\UI;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Admin\Courses;

use GuzzleHttp\Client;
use Illuminate\Contracts\Session\Session as SessionSession;
use Session;

class HomeController extends Controller
{



    public function home(Request $request){
        $title = "Home";

        $GetCourses= json_decode(file_get_contents("https://onlinelms.skillsgroom.com/api/course_list/7"), true);

        // echo json_encode($GetCourses['data']);
        // exit;

        $Courses = $GetCourses['data'];

        $GetTeachers = json_decode(file_get_contents("https://onlinelms.skillsgroom.com/api/teachers_list"), true);



        $Teachers = $GetTeachers['data'];

        $client = new Client();

        $response = $client->request('GET', 'https://www.udemy.com/api-2.0/courses?title=Design, Development, Finance %26 Accounting,  IT %26 Software,  Marketing, Music,  Personal Development, Photography %26 Video, Teaching %26 Academics', [
            'headers' => [
                'Accept' => 'application/json, text/plain, */*',
                'Authorization'     => 'Basic ZVZVV3h3b1J6eERUeWxzaWFJWFdDeHJPc0l6dFQxRUhNdjhPaXdvUjp3WU16NWVQYnZpNU1DTlg4TnQ5UDBNWFdZQ2E3SVZNSFhBS0tTVDJWeWtBV0dWWTMxZGk5OFBXUWhpcEg0Z0NHZmZEQWVFbmhQYktYdTZUNEpSeTJxYUI3bk9NZ2tMeXBpNmZDSk9zVlFLZHFFYVdrREJscjhEeU5leEdXVVdhNw==',
                'Content-Type'      => 'application/json;charset=utf-8',
            ]
        ]);

        $response_data = json_decode((string) $response->getBody(), true);
        $udemy_courses =  collect($response_data["results"]);


        $clients = new Client();

        $responses = $clients->request('GET', 'https://skillsgroom.talentlms.com/api/v1/courses/', [
            'headers' => [
                'Accept' => 'application/json, text/plain, */*',
                'Authorization'     => 'Basic dnRrdmVtTmJwZEVQdXFqYVFmNnNrcE15NXpVYmNhOg==',
                'Content-Type'      => 'application/json;charset=utf-8',
            ]
        ]);

        $responses_data = json_decode((string) $responses->getBody(), true);
        // $lms_courses =  collect($responses_data["results"]);

        // echo json_encode($responses_data);
        // exit;
        // $Courses = Courses::select('courses.id', 'admin.id as TeacherId', 'courses.course_name', 'admin.name', 'courses.type','courses.fees', 'courses.featured_image')->join('admin','admin.id', '=', 'courses.teacher_id')->where('courses.status', 1)->orderBy('courses.created_at', 'DESC')->get();

        return view('UI.home', compact('Courses', 'Teachers', 'udemy_courses', 'responses_data'));
    }



    public function partners(){
        $title = "Home";

        return view('UI.partners');
    }

    public function book_demo($course_id, $id){
        $title = "Book Demo";

        $Courses = Courses::where('id', $course_id)->first();
        return view('UI.book_demo');
    }

    public function login(){
        $title = "Login";

        return view('UI.login');
    }

    public function register(){
        $title = "Register";

        return view('UI.register');
    }

    public function user_login(Request $request){
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://skillsgroom.talentlms.com/api/v1/userlogin",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => array('login' => $request->username,'password' => $request->password),
          CURLOPT_HTTPHEADER => array(
            "Authorization: Basic dnRrdmVtTmJwZEVQdXFqYVFmNnNrcE15NXpVYmNhOg==",
            "Cookie: PHPSESSID=gpim28k7a6sckdu2c3gtd77ev2"
          ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        $jsonData = json_decode($response,true);

        // echo json_encode($jsonData['user_id']);
        // exit;

        // $request->session()->put('UserEmail', $response['email']);
        $request->session()->put('UserId', $jsonData['user_id']);

        return redirect('/');
    }

    public function course_list(){
        $title = "Course List";

        $GetCourses= json_decode(file_get_contents("https://onlinelms.skillsgroom.com/api/course_list/7"), true);

        $Courses = $GetCourses['data'];

        $client = new Client();

        $response = $client->request('GET', 'https://www.udemy.com/api-2.0/courses?title=Design, Development, Finance %26 Accounting,  IT %26 Software,  Marketing, Music,  Personal Development, Photography %26 Video, Teaching %26 Academics', [
            'headers' => [
                'Accept' => 'application/json, text/plain, */*',
                'Authorization'     => 'Basic ZVZVV3h3b1J6eERUeWxzaWFJWFdDeHJPc0l6dFQxRUhNdjhPaXdvUjp3WU16NWVQYnZpNU1DTlg4TnQ5UDBNWFdZQ2E3SVZNSFhBS0tTVDJWeWtBV0dWWTMxZGk5OFBXUWhpcEg0Z0NHZmZEQWVFbmhQYktYdTZUNEpSeTJxYUI3bk9NZ2tMeXBpNmZDSk9zVlFLZHFFYVdrREJscjhEeU5leEdXVVdhNw==',
                'Content-Type'      => 'application/json;charset=utf-8',
            ]
        ]);

        $response_data = json_decode((string) $response->getBody(), true);
        $udemy_courses =  collect($response_data["results"]);

        return view('UI.course_list', compact('Courses', 'udemy_courses'));
    }


    public function course_details($id){
        // $Teachers = Authendication::where('user_type', 2)->get();
        $GetCourses= json_decode(file_get_contents("https://onlinelms.skillsgroom.com/api/course_details/".$id), true);

        $Courses = $GetCourses['data'];

        // echo json_encode($Courses);
        // exit;

        // $Courses = Courses::where('id', $id)->first();
        return view('UI.course_details', compact('Courses'));

    }

    public function udemy_course_details($id){
        // $Teachers = Authendication::where('user_type', 2)->get();
        $client = new Client();

        $response = $client->request('GET', 'https://www.udemy.com/api-2.0/courses/'.$id.'?fields[course]=@all', [
            'headers' => [
                'Accept' => 'application/json, text/plain, */*',
                'Authorization'     => 'Basic ZVZVV3h3b1J6eERUeWxzaWFJWFdDeHJPc0l6dFQxRUhNdjhPaXdvUjp3WU16NWVQYnZpNU1DTlg4TnQ5UDBNWFdZQ2E3SVZNSFhBS0tTVDJWeWtBV0dWWTMxZGk5OFBXUWhpcEg0Z0NHZmZEQWVFbmhQYktYdTZUNEpSeTJxYUI3bk9NZ2tMeXBpNmZDSk9zVlFLZHFFYVdrREJscjhEeU5leEdXVVdhNw==',
                'Content-Type'      => 'application/json;charset=utf-8',
            ]
        ]);

        $response_data = json_decode((string) $response->getBody(), true);
        $Courses =  collect($response_data);



        // echo json_encode($Courses['title']);
        // exit;

        // $Courses = Courses::where('id', $id)->first();
        return view('UI.udemy_course_details', compact('Courses'));

    }

    public function lms_course_details($id){
        // $Teachers = Authendication::where('user_type', 2)->get();
        $client = new Client();

        $response = $client->request('GET', 'https://skillsgroom.talentlms.com/api/v1/courses/id:'.$id, [
            'headers' => [
                'Accept' => 'application/json, text/plain, */*',
                'Authorization'     => 'Basic dnRrdmVtTmJwZEVQdXFqYVFmNnNrcE15NXpVYmNhOg==',
                'Content-Type'      => 'application/json;charset=utf-8',
            ]
        ]);

        $response_data = json_decode((string) $response->getBody(), true);
        $Courses =  collect($response_data);


        // echo json_encode($Courses);
        // exit;

        // $Courses = Courses::where('id', $id)->first();
        return view('UI.lms_course_details', compact('Courses'));

    }

    public function advisor_details($id){
        // $Teachers = Authendication::where('user_type', 2)->get();
        $GetTeachers = json_decode(file_get_contents("https://onlinelms.skillsgroom.com/api/teachers_details/".$id), true);

        $Teachers = $GetTeachers['data'];

        $GetTestimonials = json_decode(file_get_contents("https://onlinelms.skillsgroom.com/api/testimonials_details/".$id), true);

        $Testimonials = $GetTestimonials['data'];

        $GetCourses = json_decode(file_get_contents("https://onlinelms.skillsgroom.com/api/course_list_basedon_teachers/".$id), true);

        $Courses = $GetCourses['data'];

        $GetVideos = json_decode(file_get_contents("https://onlinelms.skillsgroom.com/api/videos_list/".$id), true);

        $Videos = $GetVideos['data'];

        // echo json_encode($Videos);
        // exit;

        // $Courses = Courses::where('id', $id)->first();
        return view('UI.advisor_details', compact('Teachers', 'Testimonials', 'Courses', 'Videos'));

    }


    public function search_course(Request $request){
        $title = "Home";

        $Name = $request->course_name;

        $GetCourses= json_decode(file_get_contents("https://onlinelms.skillsgroom.com/api/search_course/".$Name), true);


        $Courses = $GetCourses['data'];

        $client = new Client();

        $response = $client->request('GET', 'https://www.udemy.com/api-2.0/courses?search='.$Name, [
            'headers' => [
                'Accept' => 'application/json, text/plain, */*',
                'Authorization'     => 'Basic ZVZVV3h3b1J6eERUeWxzaWFJWFdDeHJPc0l6dFQxRUhNdjhPaXdvUjp3WU16NWVQYnZpNU1DTlg4TnQ5UDBNWFdZQ2E3SVZNSFhBS0tTVDJWeWtBV0dWWTMxZGk5OFBXUWhpcEg0Z0NHZmZEQWVFbmhQYktYdTZUNEpSeTJxYUI3bk9NZ2tMeXBpNmZDSk9zVlFLZHFFYVdrREJscjhEeU5leEdXVVdhNw==',
                'Content-Type'      => 'application/json;charset=utf-8',
            ]
        ]);

        $response_data = json_decode((string) $response->getBody(), true);
        $udemy_courses =  collect($response_data["results"]);

        // echo json_encode($udemy_courses);
        // exit;


        return view('UI.search_courses', compact('Courses', 'udemy_courses'));
    }


    public function buy_course($id, $c_id){
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://skillsgroom.talentlms.com/api/v1/buycourse",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => array('user_id' => $id,'course_id' => $c_id),
          CURLOPT_HTTPHEADER => array(
            "Authorization: Basic dnRrdmVtTmJwZEVQdXFqYVFmNnNrcE15NXpVYmNhOg==",
            "Cookie: PHPSESSID=gpim28k7a6sckdu2c3gtd77ev2"
          ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        $jsonData = json_decode($response,true);

        return redirect($jsonData['redirect_url']);
        // echo json_encode($jsonData);
        // exit;

        // $Courses = Courses::where('id', $id)->first();
        // return view('UI.lms_course_details', compact('Courses'));

    }

    public function user_register(Request $request){

        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => "https://skillsgroom.talentlms.com/api/v1/usersignup",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 0,
          CURLOPT_FOLLOWLOCATION => true,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "POST",
          CURLOPT_POSTFIELDS => array('first_name' => $request->first_name,'last_name' => $request->last_name,'email' => $request->email,'login' => $request->login,'password' => $request->password),
          CURLOPT_HTTPHEADER => array(
            "Authorization: Basic dnRrdmVtTmJwZEVQdXFqYVFmNnNrcE15NXpVYmNhOg==",
            "Cookie: PHPSESSID=gpim28k7a6sckdu2c3gtd77ev2"
          ),
        ));

        $response = curl_exec($curl);

        curl_close($curl);

        // $client = new Client();

        // $response = $client->request('POST',
        //     'https://skillsgroom.talentlms.com/api/v1/usersignup',[
        //         'body' => "request_data=" . json_encode(
        //             array(
        //                 'first_name' => 'Mano',
        //                 'last_name' => 'Siva',
        //                 'email' => 'mano@gmail.com',
        //                 'login'=> 'Mano',
        //                 'password'=> 'Test@123'
        //             )
        //         ),
        // 'headers' => [
        //     'Authorization'     => 'Basic dnRrdmVtTmJwZEVQdXFqYVFmNnNrcE15NXpVYmNhOg==',
        //             'Content-Type'      => 'application/json;charset=utf-8',
        // ]
        //     ]);

        // $response_data = json_decode((string) $response->getBody(), true);
        // $udemy_courses =  collect($response_data["results"]);


        // echo json_encode($response_data);
        // exit;
        // $Courses = Courses::select('courses.id', 'admin.id as TeacherId', 'courses.course_name', 'admin.name', 'courses.type','courses.fees', 'courses.featured_image')->join('admin','admin.id', '=', 'courses.teacher_id')->where('courses.status', 1)->orderBy('courses.created_at', 'DESC')->get();
        return redirect('/register')->with('message','User Registered Successfully. Now you can login. ');;
        // return view('UI.home', compact('Courses', 'Teachers', 'udemy_courses', 'responses_data'));
    }

    public function logout(){
        Session::flush();
        return redirect('/');
    }
}
